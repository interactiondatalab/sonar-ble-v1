import React, {useState, useEffect} from 'react';
import {AppRegistry, View, Text} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
import {ThemeProvider} from 'styled-components';
import theme from '@data/theme';

const Root = (props) => {
  return (
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  );
  
}

AppRegistry.registerComponent(appName, () => Root);
