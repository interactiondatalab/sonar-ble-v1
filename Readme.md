# Sonar

## Getting started

### Install React-native


- Version of React-Native `0.59`
- *React native `0.60` comes with a lot of changes, and some libraries are not compatible with this version*

Follow react native guidelines https://facebook.github.io/react-native/docs/0.59/getting-started (`React-native CLI Quickstart`, we are not using `Expo`).

- use node 12.
- Git clone the repository
- npm install or yarn install to install libs

**For iOS**
- react-native run-ios

**For Android**
- Open android studio
- Tools/AVD Manager
- Create and run an emulator
- react-native run-android

**For iOS / enable release mode**
- Product > Edit Schema
- Choose `Release`

> Running on a device check:
> https://facebook.github.io/react-native/docs/running-on-device

## Libraries Installed

At the moment we are using.

```
  "dependencies": {
    "add": "^2.0.6",
    "axios": "^0.19.0",
    "expo-jwt": "^1.1.2",
    "mobx": "^5.13.0",
    "mobx-react": "^6.1.1",
    "prismic-javascript": "^2.0.3",
    "react": "16.8.3",
    "react-native": "0.59",
    "react-native-background-fetch": "2.0.x",
    "react-native-background-task": "^0.2.1",
    "react-native-ble-plx": "^1.0.3",
    "react-native-gesture-handler": "^1.3.0",
    "react-native-swiper": "^1.5.14",
    "react-native-vector-icons": "^6.6.0",
    "react-navigation": "^3.11.0",
    "styled-components": "^4.3.2",
  },
```

- **Axios**: http request
- **Mobx/Mobx-React**: manage data, using `observable/observer` pattern. https://github.com/mobxjs/mobx-react
- **Prismic**: to handle all static data in the app.
> *Onboarding and Text are defined in Prismic. When you see in the app something like `home.how_it_works`, it is a key that has not been defined in Prismic. So add it, save, publish, and reload the app*
> To create a new text use -> `t('home.new_key')`
- **react-native-background-fetch / react-native-background-task** - to call a background script every 15min. Go to https://github.com/jamesisaac/react-native-background-task for more information
- **react-native-ble-plx** -  Bluetooth library.
> Start by readiing https://polidea.github.io/react-native-ble-plx/#introduction
> Repo: https://github.com/Polidea/react-native-ble-plx
> Discussion about background issues: https://github.com/Polidea/react-native-ble-plx/issues/127
> You can use  **TestScreen.js** to do some test
- **react-native-swiper** - component used for the onboarding. https://github.com/leecade/react-native-swiper
- **react-native-vector-icons** - icons library https://github.com/oblador/react-native-vector-icons
- **react-navigation** - navigation. https://reactnavigation.org/
- **styled-components** - easy styling. https://www.styled-components.com/

## How it works?

First you need to learn
- React *(don't forget to take a look, to https://fr.reactjs.org/docs/hooks-intro.html )*
- React-Native https://facebook.github.io/react-native/docs/0.59/getting-started
- Styled-components https://www.styled-components.com/
- Mobx https://github.com/mobxjs/mobx-react

### index.js

this file is used for:
- Defining the first Component.
- Define the theme used for styled-components.
> In the theme, we defined some variable that can be used directly in a styled components.
> ```
> const ButtonLabel = styled.Text`
>  color: white;
>  font-size: ${props => props.theme.sizem}; // <-- here
>  text-align: center;
>`;

- Loading data from Prismic

### App.js
In app --> react-router-navigation.

You can find stack navigation
```
export const ExperimentsStack = createStackNavigator({
  Experiments: ExperimentsScreen,
  Experiment: ExperimentScreen,
});
```

Or tabs navigation
```
export const AppScreen = createBottomTabNavigator({
  Home: HomeScreen,
  Experiments: ExperimentsStack,
  Settings: SettingsStack,
  Test: TestScreen
}, {
  tabBarOptions: {
    activeTintColor: '#333',
    inactiveTintColor: '#aaa',
  },
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({focused, horizontal, tintColor}) => {
      const { routeName } = navigation.state;
      let iconName = 'home';
      if (routeName === 'Home') {
        iconName = 'home';
      }
      else if (routeName === 'Experiments') {
        iconName = 'wifi';
      }
      else if (routeName === 'Settings') {
        iconName = 'settings';
      } else {
        iconName = 'bluetooth'
      }
      return <TabIcon name={iconName} size={25}  />;
    }
  })
});
```

In order to handle authentication routing, the app, first load the `AuthLoadingScreen`. This one will redirect to `Onboarding` `Authentication` or `Home`.

```
// AuthLoadingScreen.js
export class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    await app.init();

    let destination = 'App';
    if (!app.isConnected) {
      destination = 'Authentication';
    }

    if (!app.isOnboardingSeen) {
      destination = 'Onboarding'
    }

    this.props.navigation.navigate('Test');
  };

  // Render any loading content that you like here
  render() {
    return (
      <View>
      </View>
    );
  }
}
```

### Components

In this folder, reusable components. `Button / Text / etc..`

#### Example 1 - Simple

```
import React from 'react';
import styled from 'styled-components/native';

export const TextS = styled.Text`
  font-size: ${props => props.theme.sizes};
  color: ${props => props.theme.textcolor};
`;

```

#### Example 2 - with children

```
import React from 'react';
import styled from 'styled-components/native';
import { TextS } from './Text';

const LabelWrapper = styled.View`
  border-radius: 5px;
  height: 40px;
  background-color: black;
`;

const LabelItem = styled(TextS)`
  color: white;
`;

export const Label = ({children}) => (
  <LabelWrapper>
    <LabelItem>{children}</LabelItem>
  </LabelWrapper>
)


// --> <Label>here children element</Label>
```


#### Example 3 - useState
```
import React from 'react';
import styled from 'styled-components/native';
import { TextS } from './Text';

export const Clock = ({children}) => {
  const [date, setDate] = useState(new Date().toISOString());

  useEffect(() => {
    // Each second, update date.
    const interval = setInterval(() => {
      setDate(new Date().toISOString())
    }, 1000);

    // When component will be destroy,
    // this function will be called
    return () => {
      clearInterval(interval);
    }
  }, []);

  return (
    <Text>{date}</Text>
  )
};

// --> <Clock />
```



### Services

In this folder, two important files.

#### **app.js**
Dynamic data are stored in this file using mobx.

1- Define variable using `@observable`
```
  @observable members = [];
  @observable isConnected = false;
  @observable experiments = [];
  @observable isOnboardingSeen = false;
  @observable user = {};
  @observable isSync = false;
  @observable experimentInProgress = null;
```

2- To update those data, @action, method need to be sync *(you cannot have api call)*
```

  @action
  setIsOnboardingSeen(isSeen) {
    this.isOnboardingSeen = isSeen;
  }

  @action
  setIsConnected(isConnected) {
    this.isConnected = isConnected;
  }

  @action
  setUser(user) {
    this.user = user;
  }

etc..
```

3- Api call / Async methods
```
  unsubscribeExp =  async () => {
    await api.unsubscribeExp(this.experimentInProgress.idExp);
    this.checkExperiment();
  }

  join = async () => {
    await api.subscribeExp(this.experimentInProgress.idExp);
    this.checkExperiment();
  }
```

`app.js` can be used in `Screen` component, using `observer`.

- 1- async method will make the api call
- 2- result will be set using `@action` method
- 3- `@action` will update `@observable` var
- 4- Component with `@observer` will be updated.


#### **api.js**

All api call are done in this file using `axios` module.

### Screens

In the App, we will have one `Screen` file by screen, there are all in the `screens` folder.

A basic Screen code will be :

```
import React, {useState, useEffect} from 'react';
import { observer } from 'mobx-react';
import app from '@services/app';

// observer is needed there, when @observable var changed,
// the component is updated
const ExampleScreen = observer(({navigation}) => {
  return (
    <Container>
      <Button onPress={() => app.start()}>Start</Button>
    </Container>
  );
});

// Use by react navigation to custom the Header for example.
ExampleScreen.navigationOptions = ({navigation}) => {
  const experiment = navigation.getParam('experiment')
  return {
    title: experiment.title
  }
}
```

### .babelrc

In `.babelrc`, you can define some alias,

```
{
  "plugins": [
    ["module-resolver", {
      "root": ["./"],
      "alias": {
        "@components": "./src/components",
        "@screens": "./src/screens",
        "@data": "./src/data",
        "@services": "./src/services"
      }
    }],
    [
      "@babel/plugin-proposal-decorators", {
        "legacy": true
      }
    ]
  ]
}
```

This way instead of having a relative path.
```
import api from '../../../services/api';
```
you can do
```
import api from '@services/api';
```

## TODO
- Find how to use bluetooth in background !
https://blog.expo.io/how-to-run-background-tasks-in-react-native-e1619acef48f
https://medium.com/arkulpa/ios-stay-connected-to-an-external-ble-device-as-much-as-possible-699d434846d2

- Add splashscreen / app icons
- Android -> signed app
- Download data, and **remove Prismic.** in release mode.
> *If you want your app to be working offline, you need to remove the dependency to Prismic*


## Troubleshooting

After a `react-native run-ios / run-android`.
React-native will open a terminal, with the command line
`npm start`

(If you install a new lib*, you must restart npm start, to be sure, you can.

If it is still not working
```
rm -rf node_nodules
npm install
react-native link
watchman watch-del-all
rm -rf /tmp/haste-map-react-native-packager-*
npm start -- --reset-cache
```