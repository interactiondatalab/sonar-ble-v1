import React from 'react';
import { View, Button, AsyncStorage, Alert, Text } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {
  createSwitchNavigator,
  createAppContainer,
  createStackNavigator,
  createBottomTabNavigator,
} from 'react-navigation';

import {withTheme} from 'styled-components';
import { OnboardingScreen } from './screens/onboarding/OnboardingScreen';
import { AuthLoadingScreen } from './screens/AuthLoadingScreen';
import { AuthenticationScreen } from './screens/authentication/AuthenticationScreen';
import { HomeScreen } from './screens/app/home/HomeScreen';
import { ExperimentsScreen } from './screens/app/experiments/ExperimentsScreen';
import { ExperimentScreen } from './screens/app/experiments/experiment/ExperimentScreen';
import { SettingsScreen } from './screens/app/settings/SettingsScreen';
import { MyAccountScreen } from './screens/app/settings/myaccount/MyAccount';

export const ExperimentsStack = createStackNavigator({
  Experiments: ExperimentsScreen,
  Experiment: ExperimentScreen,
});

export const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
  MyAccount: MyAccountScreen,
});

export const TabIcon = withTheme(({theme, name, size}) => {
  return <MaterialIcons name={name} size={size} color={theme.primaryColor} />
})

export const AppScreen = createBottomTabNavigator({
  Home: HomeScreen,
  Experiments: ExperimentsStack,
  Settings: SettingsStack
}, {
  tabBarOptions: {
    activeTintColor: '#333',
    inactiveTintColor: '#aaa',
  },
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({focused, horizontal, tintColor}) => {
      const { routeName } = navigation.state;
      let iconName = 'home';
      if (routeName === 'Home') {
        iconName = 'home';
      }
      else if (routeName === 'Experiments') {
        iconName = 'wifi';
      }
      else if (routeName === 'Settings') {
        iconName = 'settings';
      } else {
        iconName = 'bluetooth'
      }
      return <TabIcon name={iconName} size={25}  />;
    }
  })
});

const SonarNavigator = createSwitchNavigator({
  AuthLoading: AuthLoadingScreen,
  Authentication: AuthenticationScreen,
  Onboarding: OnboardingScreen,
  App: AppScreen
});

export default createAppContainer(SonarNavigator)