'use strict';

import { BleManager } from 'react-native-ble-plx';
import { PermissionsAndroid, Platform } from 'react-native';
import BLEPeripheral from 'react-native-ble-peripheral';
import { AppState } from 'react-native';
import app from "./app";

const bleManager = new BleManager();
var timestampStart = 0;
var timestampEnd = 0;
var listDevice = {};

const handleAppStateChange = async(nextAppState) => {
	// When foreground or background state changes, the scan may have not stop then stop it  
	await bleManager.stopDeviceScan();
	timestampEnd = Math.floor(Date.now() / 1000);
}
AppState.addEventListener('change', handleAppStateChange);

const startAdvertising = async(userNum) => {
	console.log("startAdvertising ...");
  
	BLEPeripheral.addService(app.serviceUUID, false);

	const userNumString = userNum.toString();
	BLEPeripheral.setName(userNumString);

	BLEPeripheral.start()
	.then(res => {
	  app.isAdvertising = 1;
	  console.log(res);
	}).catch(error => {
	  app.isAdvertising = 0;
	  console.log(error);
	})
}

export const stopAdvertising = async() => {
  console.log("stopAdvertising ...");
  BLEPeripheral.stop();
  app.isAdvertising = 0;
}

export const activateScan = async(taskDuration) => {
	if (await requestPermissions()) {
		if(app.isAdvertising === 0) {
        	await startAdvertising(app.user.userNum);
      	}

		timestampStart = Math.floor(Date.now() / 1000);

		setTimeout(() => {
			bleManager.stopDeviceScan();
			timestampEnd = Math.floor(Date.now() / 1000);
		}, taskDuration);
		
		console.log("Start scanning !!!! ---");
		bleManager.startDeviceScan(null, null, (error, device) => {
			if (error) {
				console.log(error.message);
			} else {
				//console.log("Scanning : " + device.id);
				if (device.serviceUUIDs !== null && device.serviceUUIDs.length === 1) {
				 	if (device.serviceUUIDs[0] === app.serviceUUID) {

						//console.log("serviceUUID : " + device.serviceUUIDs);
						//console.log("Name : " + device.localName)

						if (!(device.localName in listDevice)) {
							listDevice[device.localName] = [];
						}

						listDevice[device.localName].push(device.rssi);
					}
				}
			}
		});
	}
}

function round(value, decimals) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

function meanList(list) {
    let total = 0, i;
    for(i = 0; i < list.length; i++) 
    {
        total += list[i];
    }
    return round(total / list.length, 2);
}

function computeDistance(rssi) {
	const txPower = -59;
	const envFactor = 2;
	return Math.pow(10, (txPower - rssi) / (10 * envFactor));
}

function modeRSSI(listRSSI) { 
	let modes = [], count = [], i, rssi, maxIndex = 0;

	for(i = 0; i < listRSSI.length; i++) 
	{
	    rssi = listRSSI[i];
	    count[rssi] = (count[rssi] || 0) + 1;
	    if(count[rssi] > maxIndex) 
	    {
	        maxIndex = count[rssi];
	    }
	}

	for(i in count)
	{
	    if(count.hasOwnProperty(i)) 
	    {
	        if(count[i] === maxIndex) 
	        {
	            modes.push(Number(i));
	        }
	    }
	}

	// As it can be bimodal or multi-modal the returned result is provided as an array
	return modes;
}

export const buildProximityRecords = async(queue, tableUserNum) => {
	let name, nbDetection, avgRSSI, avgDistance;
	
	for (name in listDevice) {
		nbDetection = listDevice[name].length;
		avgRSSI = meanList(modeRSSI(listDevice[name]));
		avgDistance = computeDistance(avgRSSI);
		//avgRSSI = listDevice[name].reduce((previous, current) => current += previous) / nbDetection;
		
		console.log("Send " + [name, timestampStart, timestampEnd, avgRSSI, avgDistance, nbDetection]);
		
		const record = [name, timestampStart, timestampEnd, avgRSSI, avgDistance, nbDetection];
		queue.createJob("send-record", { record, }, { attempts: 1, timeout: 4000 });
	}

	listDevice = {};
}

const requestPermissions = async() => {
	if (Platform.OS === 'android') {
		try {
			const grantedCoarseLocation = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION);
			if (grantedCoarseLocation === PermissionsAndroid.RESULTS.GRANTED) {
				console.log('Access Coarse');
				
				try {
					const state = await bleManager.state();
					console.log("Bluetooth state : " + state);
					if (state === "PoweredOn") {
						return true;
					} else {
						return false;
					}
				} catch(e) {
					console.log("Error : " + e);
					return false;
				}
			}

			return false;

		} catch(e) {
			console.log("ici : " + e);
			return false;
		}
	}
	else {
		return true;
	}
}