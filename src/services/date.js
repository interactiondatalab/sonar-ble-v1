export const formatDuration = (ms) => {
  const s = Math.floor(ms / 1000);
  const min = Math.floor(s / 60);
  let h = Math.floor(min / 60);
  let day = Math.floor(h / 24);
  let hrest = h - day * 24;
  let minrest = min - h * 60;
  let srest = s - min * 60;
  if (hrest < 10) { hrest = `0${hrest}`};
  if (minrest < 10) { minrest = `0${minrest}`};
  if (srest < 10) { srest = `0${srest}`};

  return {day, h: hrest, min: minrest, s: srest};
}