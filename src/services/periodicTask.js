import { getExperimentInProgressFromStorage, storeExperimentInProgress} from "./localStorage"
import { notifyStartExp } from "./notifications"
import { buildProximityRecords } from "./bleScan"
import BackgroundFetch from "react-native-background-fetch";
import app from "./app";
import * as api from "./api"

//const taskDuration = 5000;
//const scanDuration = 4000; 
const taskDuration = 25000;
const scanDuration = 15000; 

const periodicTask = async(recordingFunc) => {
  if (app.isConnected) {
    app.queue.start();
    await app.listMembers();
    await app.checkExperiment();
    
    if(app.isExpOngoing()) {
      await buildProximityRecords(app.queue, app.tableUserNum);
      await recordingFunc(scanDuration);
    }
  }
}

export const setForegroundRecordingTask = async(recordingFunc) => {  
  setInterval(async() => {
    //console.log("Foreground");
    //bleManager.stopDeviceScan();
    //BLEPeripheral.stop();
    
    await periodicTask(recordingFunc);
  }, taskDuration)
}

export const setBackgroundRecordingTask = async(recordingFunc) => {
  BackgroundFetch.configure({
    minimumFetchInterval: 15,     // <-- minutes (15 is minimum allowed)
    // Android options
    stopOnTerminate: false,
    startOnBoot: true,
    requiredNetworkType: BackgroundFetch.NETWORK_TYPE_NONE, // Default
    requiresCharging: false,      // Default
    requiresDeviceIdle: false,    // Default
    requiresBatteryNotLow: false, // Default
    requiresStorageNotLow: false  // Default
  }, async() => {
    //bleManager.stopDeviceScan();
    //BLEPeripheral.stop();
    
    await periodicTask(recordingFunc);
    BackgroundFetch.finish(BackgroundFetch.FETCH_RESULT_NEW_DATA);
  }, (error) => {
    console.log("[js] RNBackgroundFetch failed to start");
  });
  BackgroundFetch.status((status) => {
    switch(status) {
      case BackgroundFetch.STATUS_RESTRICTED:
        console.log("BackgroundFetch restricted");
        break;
      case BackgroundFetch.STATUS_DENIED:
        console.log("BackgroundFetch denied");
        break;
      case BackgroundFetch.STATUS_AVAILABLE:
        console.log("BackgroundFetch is enabled");
        break;
    }
  });
}

