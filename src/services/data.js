import {AsyncStorage} from 'react-native';
import onboardingSlides from "../screens/onboarding/Onboarding.text";

let data = {};
export let theme = {};
export let onboarding = onboardingSlides.Slides;

export const t = (key) => {
  return data[key] ? data[key] : key;
}

export const setTheme = (newTheme) => {
  theme = newTheme;
}

export const setOnboarding = (items) => {
  items.forEach(item => onboarding.push(item))
}

export const setWording = (obj) => {
  obj.forEach((item) => {
    item.body.forEach(({primary: {key, value}}) => {
      data[`${item.name}.${key}`] = value;
    })
  })
}

export const getOnboarding = () => onboarding;

// export const load = () => {
//   const $data = await AsyncStorage.getItem('@data');
//   const result = JSON.parse($data);
//   theme = result.theme;
//   data = result.data;
//   onboarding = result.onboarding;
// };

// export const save = () => {
//   AsyncStorage.setItem('@data', JSON.stringify({data, theme, onboarding}));
// };

