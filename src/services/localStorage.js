import { AsyncStorage } from 'react-native';

const EXPERIMENT_IN_PROGRESS_KEY = "@experimentInProgress";
const RECORDS_KEY = "@records";

const getExperimentInProgressFromStorage = async() => {
  return await AsyncStorage.getItem(EXPERIMENT_IN_PROGRESS_KEY);
}

const storeExperimentInProgress = async(experiment) => {
  AsyncStorage.setItem(EXPERIMENT_IN_PROGRESS_KEY, JSON.stringify(experiment));
}

const removeExperimentInProgressFromStorage = async() => {
  AsyncStorage.removeItem(EXPERIMENT_IN_PROGRESS_KEY);
}

const getRecordsInStorage = async() => {
  const records = await AsyncStorage.getItem(RECORDS_KEY);
  return records ? JSON.parse(records) : records;
}

const storeRecordInStorage = async(record) => {
  const records = await AsyncStorage.getItem(RECORDS_KEY);
  await AsyncStorage.setItem(RECORDS_KEY, records ?
    JSON.stringify([...JSON.parse(records), record]) :
    JSON.stringify([record])
  );
}

// TODO : remove specific element from storage
const removeRecordsInStorage = async() => {
  await AsyncStorage.removeItem(RECORDS_KEY);
}

export {
  getExperimentInProgressFromStorage,
  storeExperimentInProgress,
  removeExperimentInProgressFromStorage,
  getRecordsInStorage,
  storeRecordInStorage,
  removeRecordsInStorage
}