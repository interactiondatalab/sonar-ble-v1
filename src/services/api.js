import { AsyncStorage } from 'react-native';
import axios from 'axios';
import app from './app';

const API = "http://igem-ties.info:7756";
//const API = "http://ec1a33d5.ngrok.io";

const axiosAPI = axios.create({
  baseURL: API,
  timeout: 3000,
});

export const api = async (url, method, data = null) => {
  const token = await AsyncStorage.getItem('token');
  
  const result = await axiosAPI({
    url: `api/${url}`,
    method,
    headers: {
      token,
    },
    data
  }).catch(() => { return null; });
  
  return result;
}

export const login = async (token) => {
  const result = await axios({
    url: `${API}/api/login`,
    method: 'POST',
    headers: {token}
  }).catch(() => { return null; });

  return result;
}

export const members = async () => {
  return api('members', 'GET');
};

export const startExp = async (duration = 0) => {
  console.log('startExp')
  return api('startExp', 'POST', {
    duration
  });
}

export const stopExp = async (idExp) => {
  console.log('stopExp')
  return api('stopExp', 'POST', {idExp});
}

// Check on-going experiment.
export const checkExp = async () => {
  console.log("check Experiment !!")
  return api('checkExp', 'GET');
}

// Accept participation at the experiment.
export const subscribeExp = async (idExp) => {
  console.log('subscribeExp')
  return api('subscribeExp', 'POST', {idExp});
}

export const unsubscribeExp = async (idExp) => {
  return api('unsubscribeExp', 'POST', {idExp});
}

export const experiments = async () => {
  return api('experiments', 'GET');
}

export const getExperimentRecords = async(idExp) => {
  return await api(`${idExp}/getRecords`, 'GET');
}

export const sendRecords = async([
  userNumRecorded,
  timeStart,
  timeEnd,
  avgRSSI,
  avgDistance,
  nbDetection
], idExp) => {
  console.log("Send record");

  return await api('sendRecords', 'POST', {
    idExp,
    userNumRecorded,
    timeStart,
    timeEnd,
    avgRSSI,
    avgDistance,
    nbDetection
  })
}