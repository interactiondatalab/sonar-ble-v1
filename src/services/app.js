import { action, observable, computed, autorun } from 'mobx';
import { AsyncStorage } from 'react-native';
import axios from 'axios';
import jwt from 'expo-jwt';
import * as api from './api'
import { notifyStartExp } from './notifications';
import { storeExperimentInProgress, removeExperimentInProgressFromStorage, getExperimentInProgressFromStorage } from "./localStorage"
import { setBackgroundRecordingTask, setForegroundRecordingTask } from "./periodicTask";
import { activateScan, stopAdvertising } from "./bleScan";
import queueFactory from 'react-native-queue';

const JWT_KEY = "Jx15xa1npx0exc7xca39bxe2fxfcx9aaeXxd0xdcxf6jxdfxc1V";

class App {
  @observable members = [];
  @observable isConnected = false;
  @observable experiments = [];
  @observable isOnboardingSeen = false;
  @observable user = {};
  @observable experimentInProgress = null;
  @observable isAdvertising = 0;

  constructor() {
    this.id = Math.random();
    this.serviceUUID = "cca2e4c3-de6b-45cc-85dc-ced34ae3ddef";
    this.characteristicUUID = "0000180a-0000-1475-8980-00805f9b34fb";
  }

  @action addMember({userID, userName, userNum, isLeader = false, bluetooth = false}) {
    // remove if exists
    const index = this.members.findIndex((item) => item.userID === userID);
    if (index !== -1) {
      this.members.splice(index, 1);
    }
    this.members.push({userID, userName, userNum, isLeader, bluetooth});
  }

  @action
  setIsOnboardingSeen(isSeen) {
    this.isOnboardingSeen = isSeen;
  }

  @action
  setIsConnected(isConnected) {
    this.isConnected = isConnected;
  }

  @action
  setUser(user) {
    this.user = user;
  }

  @action
  setExperimentInProgress({
    idExp,
    membersExp,
    startTime
  }) {
    const members = membersExp.map((userID) => {
      return this.members.find((m) => m.userID === userID);
    })
    
    this.experimentInProgress = {
      idExp,
      membersExp,
      members,
      startTime:  new Date(Math.floor(startTime * 1000))
    };
    storeExperimentInProgress(this.experimentInProgress);
  }

  @action
  clearExperimentInProgress() {
    if(this.experimentInProgress !== null) {
      stopAdvertising();
    }

    this.experimentInProgress = null;
    removeExperimentInProgressFromStorage();
  }

  @action
  setExperiments(experiments) {
    this.experiments.length = 0;
    Object.keys(experiments).forEach((expID) => {
      const exp = experiments[expID]
      exp.isCurrent = exp.isCurrent === 1;
      const members = exp.membersExp.map((userID) => {
        return this.members.find((m) => m.userID === userID);
      })

      this.experiments.push({
        ...exp,
        members,
        expID,
        dateEnd: exp.timeEnd && new Date(Math.floor(exp.timeEnd * 1000)).toLocaleDateString(),
        duration: exp.isCurrent ? 0 : exp.timeEnd - exp.timeStart
      });
    });
  }

  @action
  setExperimentRecord(expID, records) {
    const idxExperiment = this.experiments.findIndex((exp) => exp.expID === expID);
    const experiment = {...{}, ...this.experiments[idxExperiment]};
    experiment.records = records;

    this.experiments[idxExperiment] = experiment;
  }

  @computed get membersConnectedSize() {
    if (!this.isExperimentInProgress) {
      return 0;
    }
    return this.experimentInProgress.members.length;
  }

  @computed get membersSize() {
    return this.members.length;
  }

  @computed get isExperimentInProgress() {
    return this.experimentInProgress !== null;
  }

  @computed get isUserInExp() {
    return this.isExpOngoing();
  }

  @computed get expSorted() {
    return this
    .experiments
    .slice()
    .sort((a, b) => b.timeStart - a.timeStart)
    .map((item, key) => {
      return {
        ...item,
        title: `Experiment #${this.experiments.length - key}`
      }
    })
  }

  init = async () => {
    if (this.isInit) {
      return
    }
    this.isInit = true;

    // init queue
    this.queue = await queueFactory();
    this.queue.addWorker("send-record", async(id, payload) => {}, {
      //concurrency: 5,
      onStart: async(id, payload) => {
        await this.sendOrStoreRecord(payload.record);
      },
      onSuccess: async (id, payload) => {},
      onFailed: async(id, payload) => {},
      onComplete: async(id, payload) => {}
    });
    
    // isSeen
    const isSeen = await AsyncStorage.getItem('onBoardingSeen');
    this.setOnboardingSeen(isSeen && isSeen === '1');
    
    await this.checkLogin();
    
    setForegroundRecordingTask(activateScan);
    setBackgroundRecordingTask(activateScan);
  }

  listMembers = async () => {
    const result = await api.members();
    
    if (result !== null) {
      const { data: { listMembers }} = result;
      
      Object.keys(listMembers).forEach((userID) => {
        
        this.addMember({
          userID,
          userName: listMembers[userID][1],
          userNum: listMembers[userID][0]
        })
      })
    }
  };

  isExpOngoing = () => {
    if (this.experimentInProgress !== null && this.experimentInProgress.membersExp) {
      //console.log(this.experimentInProgress.membersExp, this.user)
      return this.experimentInProgress.membersExp.includes(this.user.userID);
    }
    return false;
  }

  firstLoad = async () => {
    await this.listMembers();
    this.checkExperiment();
  }

  checkLogin = async () => {
    // token
    this.token = await AsyncStorage.getItem('token');
    if (this.token) {
      this.setIsConnected(true);
    }
    else { console.log("no login !!!")
    }

    // user
    const user = await AsyncStorage.getItem('user');
    if (user) {
      this.setUser(JSON.parse(user));
    } else {
      console.log('User is not found')
    }
  }

  checkExperiment = async () => {
    const result = await api.checkExp();
    
    if (result !== null) {
      if (result.status === 200) {
        const experimentFromStorage = await getExperimentInProgressFromStorage();

        if (experimentFromStorage === null) {
          notifyStartExp(); 
        }

        this.setExperimentInProgress(result.data);
      } else if (result.status === 204) {
        this.clearExperimentInProgress();
      }
    }
  }

  setOnboardingSeen = async (isSeen = true) => {
    AsyncStorage.setItem('onBoardingSeen', isSeen ? '1' : '0');
    this.setIsOnboardingSeen(isSeen);
  }

  login = async (userID) => {
    const token = jwt.encode({userID}, JWT_KEY);
    result = await api.login(token);

    if (result !== null) {
      if (result.status === 204) {
        throw new Error('user-not-found');
        return;
      }

      const user = {
        userNum: result.data.userNum,
        userID: userID,
        userName: result.data.userName,
        isLeader: result.data.isLeader === 1
      };
      
      this.setUser(user);
      this.setIsConnected(true);

      AsyncStorage.setItem('user', JSON.stringify(user));
      AsyncStorage.setItem('token', token);
    }   
  }

  logout = async () => {
    AsyncStorage.multiRemove(['user','token']);
    this.clearExperimentInProgress();
    this.token = null;
    this.tableUserNum = {}
    this.setIsConnected(false);
  }

  startExp = async () => {
    await api.startExp();
    const result = await api.checkExp()
    if (result !== null) {
      await api.subscribeExp(result.data.idExp);
      this.checkExperiment();
    }
  }

  stopExp = async () => {
    if (await api.stopExp(this.experimentInProgress.idExp) !== null) this.checkExperiment();
  }

  unsubscribeExp =  async () => {
    if (await api.unsubscribeExp(this.experimentInProgress.idExp) !== null) this.checkExperiment();
  }

  join = async () => {
    if (await api.subscribeExp(this.experimentInProgress.idExp) !== null) this.checkExperiment();
  }

  listExp = async () => {
    const result = await api.experiments();
    if (result !== null) {
      const { data } = result;
      this.setExperiments(data);
    }
  }

  getExperimentRecords = async(idExp) => {
    const result = await api.getExperimentRecords(idExp);
    if (result !== null) {
      const records = result.data["records"];
      
      //await this.setExperimentRecord(idExp, records);
      return records;
    } else {
      return [];
    }
  }

  /**
   * Send a record. If an error occurs in sending, trigger timeout on queue's work to store it. If no error, try to send previously stored records.
   * @param {*} record 
   */
  sendOrStoreRecord = async(record) => {
    if (this.isExpOngoing()) {
      if (await api.sendRecords(record, this.experimentInProgress.idExp) === null) {
        console.log("Error !!!!!!!!!!!!");
        setTimeout(() => {
          this.queue.createJob("send-record", {
          record: record
          }, { attempts: 1, timeout: 1000 });
        }, 1000);      
      }
    }
  }

}

export default new App();
