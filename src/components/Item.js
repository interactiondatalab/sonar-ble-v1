import React from 'react';
import { TouchableHighlight } from 'react-native';
import styled, { withTheme } from 'styled-components/native';
import {TextM, TextS} from '@components/Text';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export const ListItem = styled.View`
  flex-direction: row;
  height: 50px;
  border-bottom-color: ${({theme}) => theme.bordercolor};
  border-bottom-width: ${({theme}) => theme.borderwidth};
  justify-content: space-between;
  align-items: center;
  padding: 0px 20px;
`

export const ItemNameWrapper = styled.View`
  flex-direction: column;
`;

export const ItemTitle = styled(TextM)`
  ${({active, theme}) => active && `color: ${theme.activecolor};`}
`;

export const ItemSubtitle = styled(TextS)`
  ${({active, theme}) => active && `color: ${theme.activecolor};`}
`;

export const Item = withTheme(
  ({
    theme,
    title,
    subtitle,
    icon,
    iconColor,
    onPress,
    active = false
  }) => {
    const toReturn = (
      <ListItem>
        <ItemNameWrapper>
          {title && <ItemTitle active={active}>{title}</ItemTitle>}
          {subtitle && <ItemSubtitle active={active}>{subtitle}</ItemSubtitle>}
        </ItemNameWrapper>
        {icon && iconColor && (
          <MaterialIcons
            name={icon}
            size={20}
            color={iconColor}
          ></MaterialIcons>
        )}
      </ListItem>
    );

    if (onPress) {
      return (
        <TouchableHighlight
          activeOpacity={0.8}
          underlayColor={theme.secondarybackgroundcolor}
          onPress={onPress}
        >
          {toReturn}
        </TouchableHighlight>
      );
    }

    return toReturn;
})