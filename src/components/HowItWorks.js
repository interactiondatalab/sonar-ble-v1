import React from 'react';
import styled, {withTheme} from 'styled-components/native';
import {withNavigation} from 'react-navigation';
import {Linking} from 'react-native';
import {t} from '@services/data';
import wording from '../data/wording';

const HowItWorksWrapper = styled.TouchableHighlight`
  justify-content: center;
  align-items: center;
`;

const HowItWorksText = styled.Text`
  color: ${({theme: {primarycolor}}) => primarycolor};
  padding: 10px;
`;

export const HowItWorks = withNavigation(withTheme(({theme, navigation}) => (
  <HowItWorksWrapper onPress={() => {
      navigation.navigate('Onboarding')
    }}
    activeOpacity={0.8}
    underlayColor={theme.secondarybackgroundcolor}
  >
    <HowItWorksText>
      {wording.general.how_it_works}
    </HowItWorksText>
  </HowItWorksWrapper>
)));