import React from 'react';
import styled, {withTheme} from 'styled-components/native';

const TextInputContainer = styled.TouchableHighlight`
  width: 100%;
  padding: 2px;
  margin: 10px;
  border-bottom-color: ${props => props.theme.primarycolor};
  border-bottom-width: 2px;
`;

const TextInputItem = styled.TextInput`
  color: ${props => props.theme.primarycolor};
  font-size: ${props => props.theme.sizem};
  text-align: left;
  height: 40px;
`;

export const TextInput = withTheme((props) => (
  <TextInputContainer>
    <TextInputItem
      placeholderTextColor="#333333"
      placeholder="Uolo"
      {...props}
    />
  </TextInputContainer>
));