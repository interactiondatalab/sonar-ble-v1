import React, {useState} from 'react';
import {Text} from 'react-native';
import styled, {withTheme} from 'styled-components/native';
import { TouchableHighlight } from 'react-native-gesture-handler';

const TabsWrapper = styled.View`
  height: ${props => props.height};
  width: 100%;
`;

const TabsContainer = styled.View`
  flex: 1;
`;

const TabsLabelContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  height: 40px;
  background-color: ${({theme}) => theme.secondarybackgroundcolor};;
`;

const TabLabelContainer = styled.TouchableHighlight`
  flex: 1;
  height: 40px;
  padding: 0;
  justify-content: center;
  ${({active, theme}) => active && `
    border-bottom-color: ${theme.primarycolor};
    border-bottom-width: 2px;
  `};
`;

const TabLabel = styled.Text`
  text-align: center;
  ${({active, theme}) => active && `
    color: ${theme.primarycolor};
  `};
`;

export const Tabs = withTheme(
  ({theme, label1, label2, children1, children2, height = "80px"}) => {
    const [active, setActive] = useState(0);

    function onPress(item) {
      setActive(item);
    }

    return (
      <TabsWrapper height={height}>
        <TabsLabelContainer>
          <TabLabelContainer
            onPress={() => onPress(0)}
            active={active === 0}
            activeOpacity={0.8}
            underlayColor={theme.secondarybackgroundcolor}
          >
            <TabLabel
              active={active === 0}
            >{label1}</TabLabel>
          </TabLabelContainer>
          <TabLabelContainer
            onPress={() => onPress(1)}
            active={active === 1}
            activeOpacity={0.8}s
            underlayColor={theme.secondarybackgroundcolor}
          >
            <TabLabel
              active={active === 1}
            >{label2}</TabLabel>
          </TabLabelContainer>
        </TabsLabelContainer>
        <TabsContainer>
          { active === 0 && children1 }
          { active === 1 && children2 }
        </TabsContainer>
      </TabsWrapper>
    )
});
