import React from 'react';
import styled from 'styled-components/native';

const ButtonContainer = styled.TouchableHighlight`
  background: ${props => props.theme.primarycolor};
  width: 80%;
  padding: 10px;
  margin: 10px;
  ${({grey}) => grey && `
    border-color: rgba(0, 0, 0, 0.5);
    border-radius: 20px;
    border-width: 2px;
    padding: 8px;
    width: 60%;
    background: transparent;
  `};
`;

const ButtonLabel = styled.Text`
  color: white;
  font-size: ${props => props.theme.sizem};;
  text-align: center;
  ${({grey}) => grey && `
    color: rgba(0, 0, 0, 0.5);
  `};
`;

export const Button = ({children, onPress, grey}) => (
  <ButtonContainer onPress={onPress} grey={grey}>
    <ButtonLabel grey={grey}>{children}</ButtonLabel>
  </ButtonContainer>
);