import React from 'react';
import {View, Text, PermissionsAndroid, Platform} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import styled from 'styled-components/native';
import {Button} from '@components/Button';
import {Item} from '@components/Item';
import axios from 'axios';

import { BleManager } from 'react-native-ble-plx';

const Container = styled.View`
  flex: 1;
  height: 100%;
  width: 100%;
  background: #eee;
  justify-content: center;
  align-items: center;
`;

const ScrollViewWrapper = styled.View`
  width: 100%;
  padding-top: 10px;
  flex: 1;
`

export class TestScreen extends React.Component {
  constructor(props) {
    super(props);
    this.manager = new BleManager({
      restoreStateIdentifier: 'testBleBackgroundMode',
      restoreStateFunction: bleRestoredState => {
        // axios.post('http://192.168.0.46:4042/', bleRestoredState);
      }
    });
  }

  componentWillMount() {
    // const subscription = this.manager.onStateChange((state) => {
    //     if (state === 'PoweredOn') {
    //         // this.scanAndConnect();
    //         subscription.remove();
    //     }
    // }, true);
  }

  scanAndConnect = () => {
    console.log('scan');
    this.manager.startDeviceScan(null, null, (error, device) => {
        if (error) {
            // Handle error (scanning will be stopped automatically)
            return
        }
        console.log(device)


        // Check if it is a device you are looking for based on advertisement data
        // or other criteria.
        if (device.name === 'TI BLE Sensor Tag' ||
            device.name === 'SensorTag') {

            // Stop scanning as it's not necessary if you are scanning for one device.
            this.manager.stopDeviceScan();

            // Proceed with connection.
        }
    });
  }


  render() {
    return (
      <Container>
         <Button
          onPress={() => {
            this.manager.stopDeviceScan();
          }}
        >Stop</Button>
        <Button
          onPress={this.scanAndConnect}
        >Scan</Button>
      </Container>
    )
  }


  // async componentDidMount() {
  //   await initBle();
  //   this.setState({init: true});
  //   bleEmitter.addListener('BleManagerDidUpdateState', this.handleUpdateState)
  //   bleEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscover);
  //   bleEmitter.addListener('BleManagerStopScan', this.handleStopScan);
  //   bleEmitter.addListener('BleManagerDisconnectPeripheral', this.handleDiscover);
  //   bleEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', this.handleDiscover);

  //   if (Platform.OS === 'android' && Platform.Version >= 23) {
  //     PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
  //       if (result) {
  //         console.log("Permission is OK");
  //       } else {
  //         PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
  //           if (result) {
  //             console.log("User accept");
  //           } else {
  //             console.log("User refuse");
  //           }
  //         });
  //       }
  //     });
  //   }

  // }

  // handleUpdateState = (obj) => {
  //   console.log(obj)
  // }

  // handleDiscover = (obj) => {
  //   console.log(JSON.stringify(obj, null, 2))
  //   this.setState({nb: this.state.nb + 1});
  //   const {devices} = this.state;
  //   const device = devices.find((item) => item.id === obj.id);
  //   if(device) {
  //     device.rssi = obj.rssi;
  //     return
  //   }

  //   this.setState({ devices: devices.concat(obj)})
  // }

  // handleScan = () => {
  //   this.setState({scanning: true})
  //   console.log('scam')
  //   scan([], 5, true)
  //   .then((data) => {
  //     console.log({data})
  //   }).catch((err) => {
  //     console.trace(err);
  //   })
  // }


  // handleStopScan = (arg) => {
  //   console.log('Stop', arg)
  //   this.setState({scanning: false})
  // }


  // render() {
  //   const { init, nb, scanning, devices } = this.state;
  //   return (
  //     <Container>
  //       {!init && <Text>Loading</Text>}
  //       {init && (
  //         <>
  //           <ScrollViewWrapper>
  //             <ScrollView>
  //               {
  //                 devices.map(({id, name, rssi}) => (
  //                   <Item
  //                     key={id}
  //                     title={name}
  //                     subtitle={`${id} / ${rssi}`}
  //                     icon={"bluetooth-connected"}
  //                     iconColor={"#0083fc"}
  //                   />

  //                 ))
  //               }

  //             </ScrollView>
  //           </ScrollViewWrapper>
  //           <Text>{nb}</Text>
  //           <Button
  //             onPress={this.handleScan}
  //           >
  //             {scanning ? '...' : 'Scan'}
  //           </Button>
  //         </>
  //       )}
  //     </Container>
  //   );
  // }
}
