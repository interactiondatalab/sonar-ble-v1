import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  View,
} from 'react-native';
import app from '../services/app';

export class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    await app.init();
    
    let destination = 'App';
    if (!app.isConnected) {
      destination = 'Authentication';
    }

    if (!app.isOnboardingSeen) {
      destination = 'Onboarding';
    }
    
    this.props.navigation.navigate(destination);
  };

  // Render any loading content that you like here
  render() {
    return (
      <View>
      </View>
    );
  }
}