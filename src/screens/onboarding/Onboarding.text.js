export default {
  Slides: [{
    image: require("./undraw_network.png"),
    title: "Team Interactions Experiment",
    subtitle: "Join a research project to understand team success in science.",
  },
  {
    image: require("./undraw_team.png"),
    title: "Proximity Network",
    subtitle: "With SONAR, launch and participate to team interactions recordings by measuring your proximity to other team members.",
  },
  {
    image: require("./undraw_safe.png"),
    title: "Safe & Secure",
    subtitle: "All the collected data are anonymised, kept secured on a server, and participation to each data collection is completely voluntary.",
  },
  {
    image: require("./undraw_report.png"),
    title: "Explore Your Data",
    subtitle: "After each proximity recording, get some insights from analytics about your interactions.",
    button: "Join us",
    buttonTarget: "home"
  }]
}