import styled from 'styled-components/native';

export const Card = styled.View`
  background-color: ${({theme}) => theme.primarybackgroundcolor};
  justify-content: center;
  align-items: center;
  flex: 1;
  padding: 10px;
`;

export const CardTitle = styled.Text`
  font-size: ${props => props.theme.sizel};
  font-weight: bold;
  text-align: center;
`;

export const CardDescription = styled.Text`
  padding-top: ${props => props.theme.sizem};
  font-size: 14px;
  text-align: center;
`;

export const CardImage = styled.Image`
  width: 100%;
  height: 25%;
  margin-bottom: 20px;
  resize-mode: contain;
`