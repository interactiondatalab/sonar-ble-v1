import React from 'react';
import Swiper from 'react-native-swiper';
import { Card, CardTitle, CardDescription, CardImage } from './OnboardingScreen.styled';
import OnboardingText from './Onboarding.text';
import {Button} from '@components/Button';
import {onboarding} from '@services/data';
import app from '@services/app';

export class OnboardingScreen extends React.Component {

  join = async () =>  {
    await app.setOnboardingSeen(true);
    this.props.navigation.navigate('AuthLoading');
  }

  render() {
    return (
      <Swiper
        loop={false}
        showsButtons={false}
      >
        {onboarding.map(({image, title, subtitle, button}, key) => (
          <Card key={key}>
            <CardImage
              source={image}
            />
            <CardTitle>{title}</CardTitle>
            <CardDescription>{subtitle}</CardDescription>
            { button && (
              <Button
                onPress={this.join}
              >{button}</Button>
            )}
          </Card>
        ))}
      </Swiper>
    );
  }
}
