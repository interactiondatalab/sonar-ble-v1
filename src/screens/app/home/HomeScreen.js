import React, {useEffect, useState} from 'react';
import {Alert, Linking} from 'react-native';
import { observer } from 'mobx-react';
import { ScrollView } from 'react-native-gesture-handler';
import { Tabs } from '@components/Tabs';
import { DataText, StartAdvice, Wrapper, Container } from './HomeScreen.styled';
import { HowItWorks } from '@components/HowItWorks';
import { TextM } from '@components/Text';
import { Button } from '@components/Button';
import { TextInput } from '@components/TextInput';
import { Start } from './Start';
import { Item } from '@components/Item';
import wording from "../../../data/wording";
import app from '../../../services/app';

export const HomeScreen = observer(() => {
  const [day, setDay] = useState(0);

  useEffect(() => {
    app.firstLoad();
  }, []);

  return (
    <Wrapper>
      <Container>
        <HowItWorks />
        {
          app.isExperimentInProgress &&
          app.isUserInExp && (
            <Start
              startTime={app.experimentInProgress.startTime}
            ></Start>
          )
        }
        {
          app.isExperimentInProgress &&
          !app.isUserInExp && (
            <Start
              label={wording.home.join}
              onPress={() => app.join()}
            ></Start>
          )
        }
        {
          !app.isExperimentInProgress &&
          app.user.isLeader && (
            <React.Fragment>
              <Start
                label={wording.home.start}
                onPress={() => app.startExp()}
              />
              <StartAdvice>{wording.home.advice}</StartAdvice>
            </React.Fragment>
          )
        }
        {
           !app.isExperimentInProgress &&
           !app.user.isLeader && (
            <TextM style={{textAlign: 'center'}}>{
              wording.home.join_experiment_need_leader_to_start_one
            }</TextM>
           )
        }
        {app.experimentInProgress && app.user.isLeader && (
          <Button grey onPress={() => app.stopExp()}>{wording.home.stop_experiment}</Button>
        )}

        {app.experimentInProgress && app.isUserInExp && !app.user.isLeader && (
          <Button grey onPress={() => app.unsubscribeExp()}>{wording.home.unsubscribe_experiment}</Button>
        )}

      </Container>
      <Tabs
        height="50%"
        label1={`${wording.home.team} ${app.membersConnectedSize}/${app.membersSize}`}
        label2={wording.home.data}
        children1={(
          <ScrollView>
            {
              app.isExperimentInProgress
              &&
              app.experimentInProgress.members.map(({
                userID,
                userName,
                isLeader,
                bluetooth = false
              }) => (
                <Item
                  key={userID}
                  title={`${userID === app.user.userID ? `You` : `${userName}`}`}
                  subtitle={`${isLeader ? `${wording.home.team_leader} ` : `${wording.home.team_member} `}-${bluetooth ? ` ${wording.home.connected}` : ` ${wording.home.away}`}`}
                  icon={bluetooth ? "bluetooth-connected" : "bluetooth"}
                  iconColor={bluetooth ? "#0083fc" : "#999"}
                />
              ))
            }
          </ScrollView>
          )}
        children2={(
          <DataText>
            {wording.home.data_advertisement}
          </DataText>
        )}
      >
      </Tabs>
    </Wrapper>
  );
});



// export class HomeScreen extends React.Component {

//   constructor(props) {
//     super(props);
//     this.state = {
//       label: wording.home.start')
//     }
//   }

//   start = () => {
//     Alert.alert(
//       'Turn on bluetooth',
//       'Turn on bluetooth to allow "SONAR" to connect to your device',
//       [
//         {
//           text: 'Settings',
//           onPress: () => {
//             Linking.canOpenURL('App-Prefs:root=Bluetooth').then(supported => {
//               if (!supported) {
//                 console.log('Can\'t handle settings url');
//               } else {
//                 return Linking.openURL('App-Prefs:root=Bluetooth');
//               }
//             }).catch(err => console.error('An error occurred', err));
//           }
//         },
//         {
//           text: 'Ok',
//           onPress: () => console.log('Ask me later pressed'),
//       },
//       ],
//       {cancelable: false}
//     )
//   }

//   render() {
//     const {label} = this.state;


//   }
// }
