import styled from 'styled-components/native';
import {TextM, TextS, TextInfo} from '@components/Text';


export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  background: ${({theme}) => theme.secondarybackgroundcolor};
`

export const Wrapper = styled.View`
  width: 100%;
  height: 100%;
`

export const DataText = styled(TextM)`
  text-align: center;
  padding: 20px;
  color: #AAAAAA;
`;

export const StartAdvice = styled(TextInfo)`
  padding: 5px;
`

export const ButtonStop = styled.Button`
  border-radius: 5px;
`;