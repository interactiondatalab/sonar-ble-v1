import React, { useState, useEffect } from 'react';
import styled from 'styled-components/native';
import { TouchableHighlight } from 'react-native';
import {formatDuration} from '@services/date';

const StartWrapper = styled.View`
  width: 100%;
  justify-content: center;
  align-items: center;
`;

const StartCircle = styled.TouchableHighlight`
  width: ${({theme: {startbuttonsize}}) => startbuttonsize};
  height: ${({theme: {startbuttonsize}}) => startbuttonsize};
  border-radius: ${({theme: {startbuttonsize}}) => startbuttonsize};
  ${({
    isWhite,
    theme: {
      startbuttonbackgroundcolor,
      startbordercolor,
      startborderwidth
    }
  }) => {
    return `
      background-color: ${isWhite ? "white" : startbuttonbackgroundcolor};
      border-width: ${startborderwidth};
      border-color: ${startbordercolor};
    `
  }}
  justify-content: center;
  align-items: center;
`;

const StartText = styled.Text`
  text-transform: uppercase;
  font-size: ${({theme}) => theme.sizexl};
  font-weight: bold;
  color: ${({theme}) => theme.startbuttontextcolor};
`;

export const Start = ({
  label,
  onPress,
  startTime
}) => {
  const [text, setText] = useState(label);
  const [start, setStart] = useState(Date.now());

  useEffect(() => {
    if (startTime) {
      const ms = start - startTime;
      const { h, min, s} = formatDuration(ms);
      setText (`${h}:${min}:${s}`);
    }
  }, [startTime, label, start]);

  useEffect(() => {
    const interval = setInterval(() => {
      setStart(Date.now())
    }, 1000);
    return () => {
      clearInterval(interval);
    }
  }, []);

  return (
    <TouchableHighlight onPress={onPress}>
      <StartWrapper>
        <StartCircle
          activeOpacity={0.8}
          isWhite={startTime > 0}
          onPress={onPress}
        >
          <StartText>{text}</StartText>
        </StartCircle>
      </StartWrapper>
    </TouchableHighlight>

  )
}