import React from 'react';
import styled from 'styled-components/native';
import {TextM, TextInfo} from '@components/Text';

export const Container = styled.View`
  width: 100%;
  height: 100%;
  justify-content: space-between;
  background-color: ${({theme}) => theme.primarybackgroundcolor};
`;

export const DateWrapper = styled.View`
  height: 30%;
  justify-content: space-evenly;
  align-items: center;
  background: ${({theme}) => theme.secondarybackgroundcolor};
`

export const Date = styled(TextM)`

`;

export const DurationLabel = styled(TextInfo)`

`;

export const Duration = styled(TextM)`
  font-weight: bold;

`;