import React, {useState, useEffect} from 'react';
import { Text, View, Button} from 'react-native';
import { Tabs } from '@components/Tabs';
import { ScrollView } from 'react-native-gesture-handler';
import { Item } from '@components/Item';
import {
  Container,
  DateWrapper,
  Date,
  DurationLabel,
  Duration
} from "./ExperimentScreen.styled";
import {t} from '@services/data';
import app from '@services/app';
import {formatDuration} from '@services/date';
import wording from "../../../../data/wording";
import RecordsCharts from "./RecordsCharts";

export const ExperimentScreen = ({navigation}) => {
  const experiment = navigation.getParam('experiment');
  const {
    dateEnd,
    duration,
    expID,
    membersExp,
    members,
    timeEnd,
    timeStart,
    title,
    userIDLeaderExp
  } = experiment;


  const [records, setRecords ] = useState([]);
  
  useEffect(() => { 
    const fetch = async() => {
      setRecords(await app.getExperimentRecords(expID));
    }
    fetch();
  }, []);

  const { h, min, s } = formatDuration(Math.floor(duration * 1000));

  return (
    <Container>
      <DateWrapper>
        <Date>{dateEnd}</Date>
        <DurationLabel>{wording.experiments.duration}</DurationLabel>
        <Duration>{`${h}h ${min}min ${s}s`}</Duration>
      </DateWrapper>
      <Tabs
        height="70%"
        label1={`${wording.experiments.team} ${membersExp.length}/${app.members.length}`}
        label2={wording.experiments.data}
        children1={(
          <ScrollView>
            {
              members.map((member, key) => (
                <Item
                  key={key}
                  title={member.userName}
                  subtitle={member.userID === userIDLeaderExp && 'Leader'}
                />
              ))
            }
          </ScrollView>
          )}
        children2={(
          <ScrollView>
            <RecordsCharts 
              records={records}
            />
          </ScrollView>
        )}
      >
      </Tabs>
    </Container>
  )
};

ExperimentScreen.navigationOptions = ({navigation}) => {
  const experiment = navigation.getParam('experiment');

  return {
    title: experiment.title
  }
}
