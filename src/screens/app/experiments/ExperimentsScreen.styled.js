import React from 'react';
import styled from 'styled-components/native';
import {TextM} from '@components/Text';

export const Container = styled.View`
  width: 100%;
  flex: 1;
  background-color: ${({theme}) => theme.primarybackgroundcolor};
`;

export const InstructionsWrapper = styled.View`
  width: 100%;
  height: 40px;
  justify-content: center;
  align-items: center;
  background-color: ${({theme}) => theme.secondarybackgroundcolor};
  border-bottom-color: ${({theme}) => theme.bordercolor};
  border-bottom-width: ${({theme}) => theme.borderwidth};
`;

export const Instructions = ({children}) => (
  <InstructionsWrapper>
    <TextM>{children}</TextM>
  </InstructionsWrapper>
)
