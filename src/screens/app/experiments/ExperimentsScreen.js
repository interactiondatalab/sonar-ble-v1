import React, { useEffect } from 'react';
import { View} from 'react-native';
import { observer } from 'mobx-react';
import { ScrollView } from 'react-native-gesture-handler';
import { Item } from '@components/Item';
import { Button } from '@components/Button';
import { Container, Instructions } from './ExperimentsScreen.styled';
import app from '../../../services/app';
import wording from "../../../data/wording";

export const ExperimentsScreen = observer(({navigation}) => {

  useEffect(() => {
    app.listExp();
  }, [])

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Instructions>{wording.experiments.find_all_experiments}</Instructions>
        <Container>
          <ScrollView>
            {
              app.expSorted.map((experiment) => {
                let subtitle = "";

                if (experiment.isCurrent) {
                  subtitle = `${wording.experiments.ongoing}`
                } else {
                  subtitle = `${wording.experiments.finished} - ${experiment.dateEnd}`
                }
                
                return (
                  <Item
                    onPress={() => {
                      if (!experiment.isCurrent) {
                        navigation.navigate('Experiment', {experiment})
                      } else {
                        navigation.navigate('Home')
                      }
                    }}
                    key={experiment.expID}
                    title={experiment.title}
                    subtitle={subtitle}
                    icon={"chevron-right"}
                    iconColor={"#999"}
                    active={experiment.isCurrent}
                  />
                )
              })
            }
          </ScrollView>
        </Container>
        <Button
          onPress={() => navigation.navigate('Home')}
        >{wording.experiments.start_new_experiment}</Button>
      </View>
  )
});

ExperimentsScreen.navigationOptions = {
  title: 'Experiments'
};
