import React from 'react';
import {Container} from './MyAccount.styled';
import { Item } from '@components/Item';
import app from '../../../../services/app';

export const MyAccountScreen = ({navigation}) => {
  async function logout() {
    navigation.navigate('AuthLoading');
    app.logout();
  }

  return (
    <Container>
      <Item
        title={'Logout'}
        icon="power-settings-new"
        iconColor="red"
        onPress={logout}
      />
    </Container>
  );
}

MyAccountScreen.navigationOptions = {
  title: "My Account"
}
