import React from 'react';
import { observer } from 'mobx-react';
import { ScrollView } from 'react-native-gesture-handler';
import { Item } from '@components/Item';
import { TextM } from '@components/Text';
import { Container } from './SettingsScreen.styled';
import wording from "../../../data/wording";

const settingsData = [
  { id: 1, label: wording.settings.my_account, path: "MyAccount"},
  { id: 2, label: wording.settings.my_igem_team, path: "Onboarding"},
  { id: 3, label: wording.settings.about, path: "Onboarding"},
  { id: 4, label: wording.settings.tutorial, path: "Onboarding"},

]

export const SettingsScreen = ({navigation}) => {
  return (
    <Container>
      <ScrollView>
        {
          settingsData.map(({id, label, path}) => (
            <Item
              key={id}
              title={label}
              icon="chevron-right"
              iconColor="#aaaaaa"
              onPress={() => navigation.navigate(path)}
            />
          ))
        }
      </ScrollView>
    </Container>
  );
};

SettingsScreen.navigationOptions = {
  title: wording.settings.title
};
