import styled from 'styled-components/native';

export const Logo = styled.Image`
  width: 80%;
  height: 20%;
  margin-bottom: 20px;
  resize-mode: contain;
`

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 20px;
  background-color: ${({theme}) => theme.primarybackgroundcolor};

`
