import React, {useState} from 'react';
import { View } from 'react-native';
import {Logo, Container} from './AuthenticationScreen.styled';
import {TextL, TextInfo, TextError} from '@components/Text';
import {TextInput} from '@components/TextInput';
import {Button} from '@components/Button';
import app from '../../services/app';
import wording from '../../data/wording';

export const AuthenticationScreen = ({navigation}) => {
  const [userID, setUserID] = useState(null);
  const [error, setError] = useState(null);

  async function join() {
    try {
      await app.login(userID);
      navigation.navigate('Home');
    } catch(err) {
      console.log(err);
      setError(wording.login.user_not_found)
    }
  }

  return (
    <Container>
      <Logo source={require('@data/sonar.png')}></Logo>
      <TextL>{wording.login.title}</TextL>
      <TextInput
        onChangeText={setUserID}
        value={userID}
        autoCapitalize="none"
        placeholder={wording.login.label_user}
      ></TextInput>
      <TextInfo>{wording.login.subtitle}</TextInfo>
      <Button
        onPress={join}
      >{wording.login.button}</Button>
      { error && <TextError>{error}</TextError>}
    </Container>
  );
}
