export default {
  home: {
    title: "Home",
    start: "Start",
    team: "Team",
    data: "Data",
    away: "Away",
    join: "Join",
    connected: "Connected",
    data_advertisement: "Summary data will appear here once the recording is over.",
    team_leader: "Representative",
    team_member: "Member",
    stop_experiment: "Stop experiment",
    unsubscribe_experiment: "Unsubscribe to experiment",
    join_experiment_need_leader_to_start_one: "To join an experiment, your Team Representative needs to start one",
    advice: "Click START to begin recording your team interactions"
  },
  experiments: {
    title: "Recordings",
    find_all_experiments: "All proximity recordings",
    finished: "Ended",
    duration: "Duration",
    team: "Team",
    data: "Data",
    data_not_available: "Summary data will appear here once the recording is over.",
    start_new_experiment: "Start a new experiment",
    interactions_with_team_members: "Interactions with team members",
    total_interactions: "Total interactions",
    total_number_of_interactions: "Total number of interactions"
  },
  settings: {
    title: "Settings",
    my_account: "My account",
    my_igem_team: "My IGEM team",
    about: "About",
    tutorial: "Tutorial"
  },
  login: {
    title: "Welcome to iGEM TIES",
    subtitle: "To sign in, use your iGEM User ID and password you used for registration",
    label_user: "IGEM User ID",
    label_email: "Email",
    button: "Join",
    join_experiment_need_leader_to_start_one: "To join an experiment, your Team Representative needs to start one",
    user_not_found: "User not found"
  },
  general: {
    how_it_works: "How does it work?",
  }
}