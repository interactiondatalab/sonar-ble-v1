export default {
  primarybackgroundcolor: "#fff",
  secondarybackgroundcolor: "#E7EAFF",
  primarycolor: "#2F0092",
  activecolor: "#8DDAAB",
  textcolor: "#250E0E",
  textinfocolor: "#75736E",
  borderwidth: 1,
  bordercolor: "#cccccc",
  sizexs: "10px",
  sizes: "12px",
  sizem: "16px",
  sizel: "18px",
  sizexl: "24px",
  // TODO
  startbuttonsize: 120,
  startbuttonbackgroundcolor: "#7CE6A6",
  startbuttontextcolor: "#2A3826",
  startbordercolor: "#008020",
  startborderwidth: 10
}